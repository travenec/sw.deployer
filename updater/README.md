```shell
docker run \
    -e HUB_ADDRESS=https://<user>:<pass>@<your_server_here>/database \
    -v /var/run/docker.sock:/var/run/docker.sock \
    registry.gitlab.com/travenec/sw.deployer/updater
```