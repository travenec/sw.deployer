const fs = require('fs');

const loadJsonFile = (filename, defaultData) => {
    return new Promise((resolve, reject) => {
        fs.exists(filename, exists => {
            if (!exists)
                return defaultData;

            const p = fs.readFile(filename);
            p.then(result => {
                try {
                    resolve(JSON.parse(result));
                } catch (err) {
                    reject(err);
                }
            });
            p.catch(reject)
        });
    });
}

const loadJsonFileSync = (filename, defaultData) => {
    if (!fs.existsSync(filename))
        return defaultData;

    try {
        return JSON.parse(fs.readFileSync(filename));
    } catch (err) {
        console.err(err);
        return defaultData;
    }
}

const saveJsonFile = (data, filename) => {
    return new Promise((resolve, reject) => {
        try {
            const str = JSON.stringify(data);
            fs.writeFile(filename, str, (err) => {
                if (err)
                    reject(err);
                else
                    resolve(err);
            })
        } catch (err) {
            reject(err);
        }
    });
}


module.exports = {
    loadJsonFile,
    loadJsonFileSync,
    saveJsonFile
}