const db = require('./db');
const docker = require('./docker');
const fsUtils = require('./fs');
const fs = require('fs');

const SERVER_NAME = process.env.SERVER_NAME || 'default';
const CONFIG_DIR = process.env.CONFIG_DIR || "config";
const SERVER_INFO_FILENAME = process.env.SERVER_INFO_FILENAME || "config/serverInfo.json";

// create config dir if does not exist
if (!fs.existsSync(CONFIG_DIR))
    fs.mkdirSync(CONFIG_DIR);

const applyDoc = (doc) => docker.applyDoc(doc);
const loadServerInfo = () => fsUtils.loadJsonFileSync(SERVER_INFO_FILENAME, { id: SERVER_NAME }); // je nutne pouzit sync verzi, jinak aplikace skonci driv, nez se data nactou a spusti se follow
const saveServerInfo = (serverInfo) => fsUtils.saveJsonFile(serverInfo, SERVER_INFO_FILENAME);

console.log('Updater is running.');

db.run(applyDoc, loadServerInfo, saveServerInfo);