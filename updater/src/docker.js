const yaml = require('yaml');
const fs = require('fs');
const { exec } = require('child_process');

const CONFIG_DIR = process.env.CONFIG_DIR || "config";

const applyDoc = function (doc) {

    if (!doc.type)
        return;

    if (doc.type == "service")
        deployService(doc);

    // download attachments asynchronously
    /*let filesToDownload = [];
    const atts = doc._attachments;
    const docId = doc._id;
    if (atts) {
        var filenames = Object.keys(atts);
        filesToDownload = filenames.map(fn => (callback) => downloadFile(docId, fn, callback));
    }

    async.series(
        filesToDownload,
        (err, results) => {
            if (err) {
                console.error(`Error downloading:  for ${docId}`, err)
                return;
            }

            console.log(`Downloaded files for ${docId}`, results);
        }
    );*/
}

const deployService = function (doc) {

    const composeFile = 'docker-compose.yml';

    if (!doc.configs || !doc.configs[composeFile]) {
        console.warn(`Document ${doc._id} does not contain '${composeFile}'.`);
        return;
    }

    if (!doc.stackName) {
        console.warn(`Document ${doc._id} has no stackName field.`);
        return;
    }

    const stackName = doc.stackName;
    const composeJson = doc.configs[composeFile];
    const composeYaml = yaml.stringify(composeJson);
    const revNumber = doc._rev
        .substring(0, doc._rev.indexOf('-')) // oriznout hash za pomlckou
        .padStart(4, '0'); // zarovnat na ctyri mista

    const stackDir = `${CONFIG_DIR}/${stackName}`;
    const composeFilename = `${stackDir}/docker-compose.${revNumber}.yml`;

    // create stack dir if does not exist
    if (!fs.existsSync(stackDir))
        fs.mkdirSync(stackDir);

    fs.writeFile(composeFilename, composeYaml, "utf8", (err) => {
        if (err)
            throw err;

        console.log(`Stack config for '${stackName}' saved to ${composeFilename}`);
        exec(`docker stack deploy --compose-file ${composeFilename} ${stackName}`, (error, stdout, stderr) => {
            if (error)
                console.error(`exec error: ${error}`);
            if (stderr.length > 0)
                console.error(stderr);
            if (stdout.length > 0)
                console.log(stdout);
        });
    });
}

module.exports = {
    applyDoc
}