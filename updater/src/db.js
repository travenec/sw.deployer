const FILES_STORAGE = './_data/';
const DB_URL = process.env.HUB_ADDRESS;
const fs = require('fs');
const path = require('path');

let serverInfo;

if (!DB_URL) {
    console.error("Missing HUB_ADDRESS env.");
    process.exit(-1);
}

const db = require('nano')(DB_URL);

const run = function (applyDoc, loadServerInfoSync, saveServerInfo) {

    const serverInfo = loadServerInfoSync()
    console.log('serverInfo', serverInfo);
    followChanges(applyDoc, serverInfo, saveServerInfo);
}

const followChanges = function (applyDoc, serverInfo, saveServerInfo) {
    const lastChange = serverInfo.lastChange || 0;
    const feed = db.follow({
        since: lastChange,
        view: 'deployment/services',
        include_docs: true
    });

    feed.on('change', (change) => {
        const doc = change.doc;
        serverInfo.lastChange = change.seq; // save last sequence number

        console.log('Change:', change.doc._id, change.doc._rev);

        saveServerInfo(serverInfo) // saving the number of last change BEFORE it is done
            .then(body => {
                applyDoc(doc);
            })
            .catch(err => {
                console.error("ERROR:");
                console.error(err);
                process.exit(1);
            });
    });

    feed.follow();
}

const downloadFile = function (docId, filepath, callback) {
    ensureLocalPath(docId, filepath, (downloadPath) => {
        console.log('Downloading:', downloadPath);
        db.attachment
            .getAsStream(docId, filepath)
            .on('end', () => callback(null, downloadPath))
            .on('error', e => callback(e, null))
            .pipe(fs.createWriteStream(downloadPath));
    });
}

const ensureLocalPath = function (docId, sourcePath, callback) {
    const destinationPath = path.join(FILES_STORAGE, docId, sourcePath);
    const targetDir = path.dirname(destinationPath);

    fs.exists(targetDir, (exists) => {
        if (exists)
            callback(destinationPath);

        else
            // recursive requires node 10!
            fs.mkdir(targetDir, { recursive: true }, (err) => {
                if (err)
                    throw err;

                callback(destinationPath);
            });
    });
}

module.exports = {
    run,
    downloadFile
}