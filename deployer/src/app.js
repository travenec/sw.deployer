// Input params
const args = require('minimist')(process.argv.slice(2), {
    string: ['hub', 'service', 'files', 'plain_files', 'configs']
});
if (!args.hub || !args.service) {
    console.error('Bad input parameters. Need to set hub and service.');
    console.log(args);
    process.exit(-1);
}


// Initialize procedure
const dbUrl = args.hub;
const docName = args.service;
const attachmentsFilter = args.files;
const plainFilesFilter = args.plain_files;
const configsFilter = args.configs;

// Log to the DB
const fs = require('fs');
const path = require('path');
const async = require('async');
const db = require('nano')(dbUrl);
const glob = require('glob');
const mime = require('mime-types')
const yaml = require('yaml')

getOrCreate(docName)
    .then(doc => {
        console.log('document revision is', doc._rev);

        // PLAIN FILES
        const steps = [];
        if (configsFilter) {
            steps.push((callback) => insertConfigs(configsFilter, doc)
                .catch(err => callback(err, undefined))
                .then(newDoc => { doc = newDoc; callback(undefined, newDoc); }));
        }
        if (plainFilesFilter) {
            steps.push((callback) => insertPlainFiles(plainFilesFilter, doc)
                .catch(err => callback(err, undefined))
                .then(newDoc => { doc = newDoc; callback(undefined, newDoc); }));
        }
        if (attachmentsFilter) {
            steps.push((callback) => uploadFiles(attachmentsFilter, doc)
                .catch(err => callback(err, undefined))
                .then(newDoc => { doc = newDoc; callback(undefined, newDoc); }));
        }

        // wait for all files to be uploaded
        async.series(steps, (err, res) => {
            if (err) {
                console.error(err);
                // process.exit(-1);
                reject(err);
                return;
            }

            console.log('EOK.', doc._rev);
        });
    })
    .catch(error => {
        console.error(error);
        process.exit(-1);
    });


function getOrCreate(docName) {
    return new Promise((resolve, reject) => {
        db.get(docName)
            .then(resolve)
            .catch((error) => {
                db.insert({ _id: docName })
                    .then((doc) => {
                        resolve({
                            _id: doc.id,
                            _rev: doc.rev
                        });
                    })
                    .catch(reject);
            });
    });
}

function promiseAllFiles(filesFilter, mapFunc) {
    return new Promise((resolve, reject) => {
        glob(filesFilter, { nodir: true }, function (er, files) {

            // create async upload functions for each file
            const fns = files.map(mapFunc);

            // wait for all files to be uploaded
            async.series(fns, (err, res) => {
                if (err) {
                    console.error(err);
                    // process.exit(-1);
                    reject(err);
                    return;
                }

                const doc = Array.from(res).pop(); // last item of res
                console.log('Attachments EOK.', doc._rev);
                resolve(doc);
            });
        });
    });
}

function uploadFiles(filesFilter, doc) {
    return promiseAllFiles(filesFilter, filepath => (callback) => {
        process.stdout.write(`Uploading '${filepath}'... `);

        fs.readFile(filepath, (err, data) => {
            if (err) {
                callback(err, undefined);
                return;
            }

            if (data.length == 0) {
                process.stdout.write("empty, skipping\n");
                callback(undefined, doc);
                return;
            }

            const mimeType = mime.lookup(filepath);

            db.attachment
                .insert(docName, filepath, data, mimeType, { rev: doc._rev })
                .catch(err => callback(err, undefined))
                .then((updateRes) => {
                    doc._rev = updateRes.rev;
                    process.stdout.write("OK\n");
                    callback(undefined, doc);
                });

        });
    });
}


function insertPlainFiles(filesFilter, doc) {
    return promiseAllFiles(filesFilter, filepath => (callback) => {
        process.stdout.write(`Reading '${filepath}'... `);

        fs.readFile(filepath, "utf8", (err, data) => {
            if (err) {
                callback(err, undefined);
                return;
            }

            if (!doc.files)
                doc.files = {};
            doc.files[filepath] = data;

            db.bulk({ docs: [doc] })
                .catch(err => callback(err, undefined))
                .then((updateRes) => {
                    doc._rev = updateRes[0].rev; // prepisovani parametru funkce, bleh...
                    process.stdout.write("OK\n");
                    callback(undefined, doc);
                });

        });
    });
}

function insertConfigs(filesFilter, doc) {
    return promiseAllFiles(filesFilter, filepath => (callback) => {
        process.stdout.write(`Reading '${filepath}'... `);

        fs.readFile(filepath, "utf8", (err, data) => {
            if (err) {
                callback(err, undefined);
                return;
            }

            const ext = path.extname(filepath);
            if (ext == ".yml")
                data = yaml.parse(data);
            else {
                callback("Do not know how to parse config data.", undefined);
                return;
            }

            if (!doc.configs)
                doc.configs = {};
            doc.configs[filepath] = data;

            db.bulk({ docs: [doc] })
                .catch(err => callback(err, undefined))
                .then((updateRes) => {
                    doc._rev = updateRes[0].rev; // prepisovani parametru funkce, bleh...
                    process.stdout.write("OK\n");
                    callback(undefined, doc);
                });
        });
    });
}